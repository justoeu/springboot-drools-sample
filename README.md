### Drools Sample

This project is a POC using Spring Boot and Drools. In this case, Drools with BRMS.
Inspired in the Sample project [www.baeldung.com](https://www.baeldung.com), I did several improvements to create dynamic Rules using application.yml with base to generate temporary files.
Furthermore, I created a sample using Table Decision.

I not test in env production, but... 

## Versions
```
        <drools-version>7.13.0.Final</drools-version>
        <lombok.version>1.18.4</lombok.version>
        <springboot.version>2.1.0.RELEASE</springboot.version>
```

## Use

* Simples Rules - http://localhost:8080/discount?type=gold
* Table Decision - http://localhost:8080/discount/xls?type=top5products&asset=top50skus&ctype=gold

## Relevant Links

   1. [Decision Tables using XLS](https://www.baeldung.com/drools-excel) 
   2. [Drools](http://www.drools.org/) 
   3. [Introduction Ro Drools](http://www.baeldung.com/drools) 
   4. [Other example using Rules from Excel Files](http://www.baeldung.com/drools-excel) 