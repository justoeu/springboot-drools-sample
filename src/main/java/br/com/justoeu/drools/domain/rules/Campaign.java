package br.com.justoeu.drools.domain.rules;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Campaign {

    private CampaignType campaignType;
    private CampaignAssets campaignAssets;
    private List<ClientLevel> clientLevel = new ArrayList<>();

    public enum CampaignType {

        TOP5PRODUCTS,
        BESTSELLERS,
        TICKET50PERCENTOFF;

        public static CampaignType toEnum(String type){
            return Arrays.stream(CampaignType.values()).filter(cp -> cp.name().equals(type)).findFirst().orElse(null);
        }

     }

    public enum CampaignAssets {

        TOP50SKUS,
        SPORTSSTORE30OFF,
        FASHIONSTORE30OFF,
        FIVEOFF,
        FIFTYOFF;

        public static CampaignAssets toEnum(String assets){
            return Arrays.stream(CampaignAssets.values()).filter(cp -> cp.name().equals(assets)).findFirst().orElse(null);
        }

    }

    public enum ClientLevel {

        BRONZE,
        SILVER,
        GOLD,
        DIAMOND;

        public static ClientLevel toEnum(String level){
            return Arrays.stream(ClientLevel.values()).filter(cp -> cp.name().equals(level)).findFirst().orElse(null);
        }
    }

    public void addLevel(ClientLevel level){
        clientLevel.add(level);
    }
}
