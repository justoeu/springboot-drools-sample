package br.com.justoeu.drools.domain.rules;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Arrays;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Offer {

    private ProductPackage financialPackage;
    private int discount;

    public enum ProductPackage {
        ADVICEFAMILY_PACKAGE,
        GOLDENYEARS_PACKAGE,
        FAMILY_PACKAGE,
        EMPLOYEE_PACKAGE,
        BUSINESS_PACKAGE;

        public static ProductPackage toEnum(String productPack){
            return Arrays.stream(ProductPackage.values()).filter(pp -> pp.name().equals(productPack)).findFirst().orElse(null);
        }

    }
}
