package br.com.justoeu.drools.web;

import br.com.justoeu.drools.domain.Product;
import br.com.justoeu.drools.domain.rules.Campaign;
import br.com.justoeu.drools.domain.rules.Offer;
import br.com.justoeu.drools.usecases.ApplyProductDiscount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductWS {

    private final ApplyProductDiscount useCase;

    @Autowired
    public ProductWS(ApplyProductDiscount applyProductDiscount){
        this.useCase = applyProductDiscount;
    }

    @RequestMapping(value = "/discount", method = RequestMethod.GET, produces = "application/json")
    public Product getDiscount(@RequestParam() String type) {

        Product product = new Product();
        product.setType(type);
        useCase.applyDiscount(product);

        return product;
    }

    @RequestMapping(value = "/discount/xls", method = RequestMethod.GET, produces = "application/json")
    public Offer getDiscountByXLS(@RequestParam() String type, @RequestParam() String asset, @RequestParam() String ctype) {

        Campaign campaign = new Campaign();
        campaign.setCampaignAssets(Campaign.CampaignAssets.toEnum(!StringUtils.isEmpty(asset) ? asset.toUpperCase() : ""));
        campaign.setCampaignType(Campaign.CampaignType.toEnum(!StringUtils.isEmpty(type) ? type.toUpperCase() : ""));
        campaign.addLevel(Campaign.ClientLevel.toEnum(!StringUtils.isEmpty(ctype) ? ctype.toUpperCase() : ""));

        return useCase.applyDiscount(campaign);
    }


}
