package br.com.justoeu.drools.usecases;

import br.com.justoeu.drools.domain.Product;
import br.com.justoeu.drools.domain.rules.Campaign;
import br.com.justoeu.drools.domain.rules.Offer;

public interface ApplyProductDiscount {

    Product applyDiscount(Product product);
    Offer applyDiscount(Campaign campaign);
}
