package br.com.justoeu.drools.usecases.impl;

import br.com.justoeu.drools.domain.Product;
import br.com.justoeu.drools.domain.rules.Campaign;
import br.com.justoeu.drools.domain.rules.Offer;
import br.com.justoeu.drools.usecases.ApplyProductDiscount;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApplyProductDiscountImpl implements ApplyProductDiscount {

    private final KieContainer kieContainer;
    private final KieSession kSession;

    @Autowired
    public ApplyProductDiscountImpl(KieContainer kieContainer, KieSession kieSession) {
        this.kieContainer = kieContainer;
        this.kSession = kieSession;
    }

    @Override
    public Product applyDiscount(Product product) {

        KieSession kieSession = kieContainer.newKieSession();
        kieSession.insert(product);
        kieSession.fireAllRules();
        kieSession.dispose();
        return product;
    }

    @Override
    public Offer applyDiscount(Campaign campaign) {

        kSession.insert(campaign);
        Offer offer = new Offer();
        kSession.setGlobal("offer", offer);
        kSession.fireAllRules();

        return offer;
    }

}
