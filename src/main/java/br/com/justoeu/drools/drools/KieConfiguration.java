package br.com.justoeu.drools.drools;

import lombok.extern.slf4j.Slf4j;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.Message;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static java.nio.charset.StandardCharsets.UTF_8;

@Slf4j
@Configuration
public class KieConfiguration {

    private final DroolsDRMProperties properties;

    @Autowired
    public KieConfiguration(DroolsDRMProperties properties){
        this.properties = properties;
    }

    @Bean
    public KieContainer kieContainer() {

        try {

            KieServices ks = KieServices.Factory.get();
            KieFileSystem kfs = ks.newKieFileSystem();

            Files.deleteIfExists(Paths.get(properties.getFileDrlPath()));

            Files.write(Paths.get(properties.getFileDrlPath()), properties.getRules().getBytes(UTF_8), StandardOpenOption.CREATE);

            kfs.write(ResourceFactory.newFileResource(properties.getFileDrlPath()));
            KieBuilder kb = ks.newKieBuilder(kfs).buildAll();

            if (kb.getResults().hasMessages(Message.Level.ERROR)) {

                throw new RuntimeException("Build Errors:\n" + kb.getResults().toString());

            }

            return ks.newKieContainer(ks.getRepository().getDefaultReleaseId());
        } catch (Exception e){
            log.error(e.getMessage(), e);
            return null;
        }
    }

    @Bean
    public KieSession kieSession(){

        try {
            KieServices ks = KieServices.Factory.get();

            Resource resource = ResourceFactory.newClassPathResource("rules/Campaigns.xls", getClass());
            KieFileSystem kfs = ks.newKieFileSystem().write(resource);

            KieBuilder kb = ks.newKieBuilder(kfs).buildAll();

            if (kb.getResults().hasMessages(Message.Level.ERROR)) {

                throw new RuntimeException("Build Errors:\n" + kb.getResults().toString());

            }

            KieRepository kieRepository = ks.getRepository();

            KieContainer kieContainer = ks.newKieContainer(kieRepository.getDefaultReleaseId());

            return kieContainer.newKieSession();
        } catch (Exception e){

            log.error(e.getMessage(),e);

            return null;
        }
    }


//    public String getDrlFromExcel(String excelFile) {
//        DecisionTableConfiguration configuration = KnowledgeBuilderFactory.newDecisionTableConfiguration();
//        configuration.setInputType(DecisionTableInputType.XLS);
//
//        Resource dt = ResourceFactory.newClassPathResource(excelFile, getClass());
//
//        DecisionTableProviderProviderImpl decisionTableProvider = new DecisionTableProviderImpl();
//
//        String drl = decisionTableProvider.loadFromResource(dt, null);
//
//        return drl;
//    }


}
