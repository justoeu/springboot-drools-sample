package br.com.justoeu.drools.drools;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Data
@Configuration
@ConfigurationProperties(prefix = "app.drools")
public class DroolsDRMProperties {

    private String rules;
    private String fileDrlPath;
    private String fileXlsPath;

}
